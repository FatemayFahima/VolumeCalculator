<?php
/**
 * Created by PhpStorm.
 * User: Fahima
 * Date: 13-04-18
 * Time: 09.58
 */

namespace Pondit\Calculator\VolumeCalculator;


class Cylinder
{
    public $radius;
    public $height;
    public function getVolume()
    {
        return 3.1416*$this->radius*$this->radius*$this->height;
    }
}