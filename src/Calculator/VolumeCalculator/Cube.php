<?php

namespace Pondit\Calculator\VolumeCalculator;


class Cube
{
    public $edge;
    public function getVolume()
    {
        return $this->edge*$this->edge*$this->edge;
    }
}