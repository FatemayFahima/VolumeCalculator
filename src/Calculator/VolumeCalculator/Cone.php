<?php
/**
 * Created by PhpStorm.
 * User: Fahima
 * Date: 13-04-18
 * Time: 09.46
 */

namespace Pondit\Calculator\VolumeCalculator;


class Cone
{
    public $radius;
    public $height;
    public function getVolume()
    {
        return 3.1416*$this->radius*$this->radius*$this->height/3;
    }
}