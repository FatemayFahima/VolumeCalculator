<?php
include_once "vendor/autoload.php";
use Pondit\Calculator\VolumeCalculator\Cube;
use Pondit\Calculator\VolumeCalculator\Displayer;

$cube1=new Cube();
$cube1->edge=5;
$displayer1 = new Displayer();
$displayer1->displaypre($cube1->getVolume());
