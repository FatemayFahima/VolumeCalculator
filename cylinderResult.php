<?php

include_once "vendor/autoload.php";
use Pondit\Calculator\VolumeCalculator\Cylinder;
use Pondit\Calculator\VolumeCalculator\Displayer;

$cylinder1=new Cylinder();
$cylinder1->radius=3;
$cylinder1->height=4;
$displayer1=new Displayer();
$displayer1->displaypre($cylinder1->getVolume());