<?php
include_once "vendor/autoload.php";
use Pondit\Calculator\VolumeCalculator\Cone;
use Pondit\Calculator\VolumeCalculator\Displayer;

$cone1=new Cone();
$cone1->radius=3;
$cone1->height=6;
$displayer1=new Displayer();
$displayer1->displaypre($cone1->getVolume());

